use anchor_lang::prelude::*;

// This is your program's public key and it will update
// automatically when you build the project.
declare_id!("Cmg6K3qtyQkHg6h2F78QhWZVbesNmG9ZnXbCGUQAwRRh");

#[program]
mod hello_anchor {
    use super::*;

    pub fn create_proposal(
        ctx: Context<CreateProposal>,
        title: String,
        description: String,
        choices: Vec<String>,
        deadline: u64,
    ) -> Result<()> {
        let proposal_account = &mut ctx.accounts.proposal;

        // Verifier < 10 choices sinon erreur
        require!(
            choices.len() <= MAX_COUNT_OF_CHOICES,
            VoteError::NotManyChoices
        );

        proposal_account.title = title;
        proposal_account.description = description;
        proposal_account.deadline = deadline;

        let mut vec_choices = Vec::new();

        for choice in choices {
            let option = Choice {
                label: choice,
                count: 0,
            };

            vec_choices.push(option);
        }

        proposal_account.choices = vec_choices;

        Ok(())
    }

    pub fn vote(ctx: Context<CastVote>, choice_index: u8) -> Result<()> {
        let proposal_account = &mut ctx.accounts.proposal;
        let voter_account = &mut ctx.accounts.voter;

        require!(
            Clock::get()?.unix_timestamp < proposal_account.deadline as i64,
            VoteError::ProposalIsOver
        );

        require!(
            proposal_account.choices.len() as u8 > choice_index,
            VoteError::ChoiceIndexOutOfScope
        );

        proposal_account.choices[choice_index as usize].count += 1;

        voter_account.choice_index = choice_index;
        voter_account.proposal = proposal_account.key();
        voter_account.user = ctx.accounts.signer.key();

        Ok(())
    }
}

const MAX_COUNT_OF_CHOICES: usize = 10;

#[derive(Accounts)]
pub struct CreateProposal<'info> {
    #[account(init, payer = signer, space = 8 + 32 + 32 + (32 + 8) * MAX_COUNT_OF_CHOICES + 8)]
    pub proposal: Account<'info, Proposal>,
    #[account(mut)]
    pub signer: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct CastVote<'info> {
    #[account(mut)]
    pub proposal: Account<'info, Proposal>,
    #[account(init, payer = signer, space = 8 + 32 + 32 + 1 + 1, seeds= [proposal.key().as_ref(), signer.key().as_ref()], bump)]
    pub voter: Account<'info, Voter>,
    #[account(mut)]
    pub signer: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[account]
pub struct Proposal {
    title: String,
    description: String,
    choices: Vec<Choice>,
    deadline: u64,
}

#[account]
pub struct Voter {
    proposal: Pubkey,
    user: Pubkey,
    choice_index: u8,
}

#[derive(AnchorSerialize, AnchorDeserialize, Clone)]
pub struct Choice {
    label: String,
    count: u64,
}

#[error_code]
pub enum VoteError {
    #[msg("Too many choices")]
    NotManyChoices,
    #[msg("Proposal is closed")]
    ProposalIsOver,
    #[msg("Choice index invalid")]
    ChoiceIndexOutOfScope,
}
